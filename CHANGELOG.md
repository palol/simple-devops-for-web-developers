# Changelog

### Version 0.1.0
- Initial test release

### Version 0.1.1
- Fixed SELinux policies
- Verified reload and provision with nfs synch
