#!/bin/bash
#
# Basic RHEL LAMP stack configuration for single site.
# Be sure all commands are uncommented if running vagrant up for the first time.
#

# Basic requirements
yum update -y
echo " "
yum install -y ntp
ntpdate time.nist.gov
systemctl start ntpd.service 
timedatectl set-timezone Europe/Zurich
yum install -y attr
yum install -y policycoreutils-python setroubleshoot

# LAMP stack deployment
# Apache2
yum install -y httpd
# MariaDB
yum install -y mariadb-server mariadb
# PHP
# Install and enable EPEL and Remi repository
yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
yum install -y http://rpms.remirepo.net/enterprise/remi-release-7.rpm
# yum-utils enables or disables repositories as well as packages without any manual configuration
yum install -y yum-utils
# Choose which version of PHP should be installed (uncomment only one of the following versions)
#yum-config-manager --enable remi-php70   # [Install PHP 7.0]
#yum-config-manager --enable remi-php71   # [Install PHP 7.1]
#yum-config-manager --enable remi-php72   # [Install PHP 7.2]
yum-config-manager --enable remi-php73   # [Install PHP 7.3]
yum install -y php php-mcrypt php-cli php-gd php-curl php-mysql php-ldap php-zip php-fileinfo
yum install -y php php5-cli php5-dev php5-fpm php5-cgi php5-mysql php5-xmlrpc php5-curl php5-gd php-apc php-pear php5-imap php5-mcrypt php5-pspell

# Add apache to vagrant group
usermod -a -G vagrant apache
groups apache

yum install -y  nfs-utils


# Start apache and mysql deamons
systemctl start httpd.service
systemctl start mariadb.service
# Enable at startup
systemctl enable httpd.service
systemctl enable mariadb.service

# SELinux configuration
setsebool -P httpd_read_user_content on
setsebool -P httpd_use_nfs on
semanage fcontext -a -t httpd_sys_content_t '/var/www/html(/.*)?'
restorecon -Rv /var/www/html

# Guest environment constants sanpshot
#history | wc
# Print server address
ip addr show eth0 | grep inet | awk '{ print $2; }' | sed 's/\/.*$//'
echo " "
# Print rules or status of firewalld
firewall-cmd --list-all
echo " "
# Print the status of SELinux
sestatus
echo " "
# Print attribute filesystem object version
getfattr --version
echo " "
# Print the PHP version
php -v
echo " "
# Print the MySQL/MariaDB version
mysql -V
echo " "
# Print the ports deamons are binded

# Print the status of the deamons
#systemctl status httpd.service
#systemctl status mariadb.service

# Print the courrent time and date of provisioning finish
DATE=`date`
UPTIME=`uptime`
echo "System finished provisioning on: $DATE"
echo "System uptime: $UPTIME"
exit 0
