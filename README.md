# Simple DevOps for web developers

Vagrant box with provisioning shell script based on vagrant centos/7 box.

### Deploy the environment

- Enable virtualization in BIOS
https://www.howtogeek.com/213795/how-to-enable-intel-vt-x-in-your-computers-bios-or-uefi-firmware/

- Install VirtualBox
https://www.virtualbox.org/wiki/Downloads

- Install Vagrant
https://www.vagrantup.com/downloads.html
https://www.vagrantup.com/docs/installation/
https://www.sitepoint.com/getting-started-vagrant-windows/

- Install git: if you are on Windows https://git-scm.com/download/win, if in nix use your package manager to install git and git-cli packages.

- Get a GitLab account
https://gitlab.com/users/sign_in#register-pane

- Fork this repo to your own account

- Clone your repo on your local machine by typing `git clone https://gitlab.com/your-repo/simple-devops-for-web-developers.git` in the terminal/shell.
- If you want to move the cloned folder to another path in your system you always can.

- Enter into the cloned folder.
- Switch to branch master by typing `git checkout master`.
- Verify you have enabled host firewall rules from and to the guest machine IP.
- Once in the project folder type `vagrant init` and wait the virtual machine to be loaded.
- Once the machine has loaded type `vagrant provision` to ensure all dependencies are installed.
- To begin developing now switch back to branch develop by typing `git checkout develop` or name a new branch and switch to it.
- Submit your changes by doing a new merge request from your Gitlab account from a non master branch to my non master branch.
- Wish all contributors happy coding!

### Community how-tos

- Send me an e-mail to be added to the developers team ( https://gitlab.com/palol/simple-devops-for-web-developers/-/project_members ).

- Git cheat sheet https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet#code

- How to Collaborate On GitHub
https://code.tutsplus.com/tutorials/how-to-collaborate-on-github--net-34267
https://www.atlassian.com/git/tutorials/comparing-workflows

- What is a pull request http://oss-watch.ac.uk/resources/pullrequest
- Best practice in release management for open source projects http://oss-watch.ac.uk/resources/releasemanagementbestpractice

### Best practices for this project

1. Do not switch off the host machine unless the guest is switched off (after `vagrant halt` finished to execute).
2. On Windows host don't put to sleep the host unless you switched off the guest (after `vagrant halt` finished to execute).
3. For developing don't use master branch.
4. If you want to run multiple instances of Vagrant VM simultaniously you have to edit the guest IP machine in the Vagrantfile.
